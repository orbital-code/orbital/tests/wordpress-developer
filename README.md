# Wordpress Developer
Para esse teste, você deve criar um plugin que insira uma opção chamada 'Cidades' no admin, onde você poderá controlar as cidades que serão exibidas no tema exibindo seu nome, temperatura e ícone, utilizando a [API do Climatempo](https://advisor.climatempo.com.br/) ou alguma outra que você se sinta confortável em utilizar.

## Requisitos
- Código bem escrito
- Não utilizar outros plugins como o ACF

## Extras
- Criar uma estrutura inicial de tema do zero

## Informações
- Disponibilizar URL com as credenciais para a visualização do teste
- O código desenvolvido deve ser disponibilizado em um repositório
- Você pode utilizar Bootstrap, Bulma ou algum outro framework que se sinta confortável
- O layout não te fará perder pontos, mas tente seguir um mockup de exemplo

## Sugestão de Layout
![Mockup](https://i.ibb.co/D5QsWXH/weather.png)
